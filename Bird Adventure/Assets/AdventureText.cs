﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureText : MonoBehaviour {

    public Text textController;
    private enum States
    {
        tree, wake, snake, dead, dead_snake, cat, run, chirp, rock, dead_cat, win
    };

    private States myState;


    // Use this for initialization
    void Start () {
        myState = States.wake;

    }
	
	// Update is called once per frame
	void Update () {

        

        if (myState == States.wake)
        {
            Wake();
        }
        else if (myState == States.tree)
        {
            Tree();
        }

        else if (myState == States.dead)
        {
            Dead();
        }

        else if (myState == States.snake)
        {
            Snake();
        }
        else if (myState == States.dead_snake)
        {
           DeadSnake();
        }
        else if (myState == States.cat)
        {
            Cat();
        }

        else if (myState == States.run )
        {
            Run();
        }
        else if (myState == States.chirp)
        {
            Chirp();
        }
        else if (myState == States.rock)
        {
            Rock();
        }
        else if (myState == States.dead_cat)
        {
            DeadCat();
        }
        else if (myState == States.win)
        {
            Win();
        }


    }

    void Wake()
    {

        textController.text = "You wake up in your bird house, but you are all alone, your family is gone. You are out of food. Your only way to survive is to get out of the house for the first time and try to get some food\n\n" +
            "Take your choice:\n" +
            "1) Get out of the bird house\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            
            myState = States.tree;
        }
    }


    void Tree()
    {

        textController.text = "Your house is on the top of the tree. You have only just started to practice flying.\n\n" +
            "Take your choice:\n" +
            "1) Try to fly down \n"+
            "2) Carefully climb down the tree.\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.dead;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myState = States.snake;
        }
    }



    void Dead()
    {

        textController.text = "You try to fly, but you crash into one of the branches. You break all your bird bones and you die.\n\n" +
            "Take your choice:\n" +
            "1) Restart game\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.wake;
        }
    }


    void DeadSnake()
    {

        textController.text = "You get eaten by a snake.\n\n" +
            "Take your choice:\n" +
            "1) Restart game\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.wake;
        }
    }

    void Snake()
    {

        textController.text = "You successfully climb down the tree and just as you climed down you see a worm in front of you. You try to get him but suddenly a wild snake appears.\n\n" +
            "Take your choice:\n" +
            "1) Just go for the worm \n" +
            "2) Stand still.\n" +
            "3) Run away.\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.dead_snake;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myState = States.cat;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            myState = States.dead_snake;
        }
    }



    void Cat()
    {

        textController.text = "You eat a worm and move on. After hours of wondering you finally spot your parens at a top of the rooftop. Unfortunately a wild cat is coming for them. Your parents see you and yell at you to run away!\n\n" +
            "Take your choice:\n" +
            "1) Run away \n" +
            "2) Start chirping.\n" +
            "3) Take a rock .\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.run;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myState = States.chirp;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            myState = States.rock;
        }
    }


    void Run()
    {

        textController.text = "You run away safely. Who knows if you will see your parents ever again\n\n" +
            "GAME OVER";
            

        
    }

    void Chirp()
    {

        textController.text = "You start chirping and cat hears you. Immeadiatly it starts running for you.\n\n" +
            "Take your choice:\n" +
            "1) Try to fly away \n" +
            "2) try to run away.\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.dead_cat;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myState = States.dead_cat;
        }
    }

    void DeadCat()
    {

        textController.text = "You give it your best but cat catches you and eats you..\n\n" +
            "Take your choice:\n" +
            "1) Restart game\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.wake;
        }
    }

    void Rock()
    {

        textController.text = "After you take a rock a cat sees and starts running towards you.\n\n" +
            "Take your choice:\n" +
            "1) Try to fly away \n" +
            "2) try to run away.\n";

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            myState = States.win;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            myState = States.dead_cat;
        }
    }


    void Win()
    {

        textController.text = "You try to fly away. After flying up the air about a meter you accidentally drop a rock on the cats head. A cat is knocked unconscious and you run away home with your parents. \n\n" +
            "GAME OVER";



    }

}
